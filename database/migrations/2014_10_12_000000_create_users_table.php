<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('cpf', 14);
            $table->string('cep', 10);
            $table->string('rua', 70);
            $table->string('bairro', 70);
            $table->string('numero');
            $table->string('tipo', 45);
            $table->string('cidade', 70);
            $table->string('estado', 2);
            $table->string('password', 45);
            $table->string('telefone', 10);
            $table->string('celular', 11);
            $table->rememberToken();
            $table->timestamps();

            $table->foreignId('vendas_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
