<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prego Digital</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="resources/css/cadastro.css">
</head>

<body>
    <header class="container fixed-top">

        <nav class="navbar navbar-expand-lg" style="background-color: #6959CD;">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php">
                    <img src="assets/img/logoPD.png" width="45" height="45" class="d-inline-block align-top" alt="">
                    <strong class="text-brand">Prego Digital</strong>
                </a>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarText">

                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="">Sobre Nós</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="">Contate-Nos</a>
                        </li>

                    </ul>

                    <span class="sessao-usuario">
                        <a class="btn" style="background-color: #836FFF;" href="cadastro.php">Crie uma conta</a>
                        <a class="btn" style="background-color: #836FFF;" href="login.php">Entrar</a>
                    </span>


                </div>
            </div>
        </nav>

        <br>
    </header>


    <main class="container">

    <br>
    <br>
    <br>

    <form class="row g-3">

        <div class="col-md-6">
            <label for="inputName" class="form-label">Nome</label>
            <input type="text" class="form-control" id="#" placeholder=" Nome Completo">
            </div>

            <div class="col-md-6">
            <label for="inputCpf" class="form-label">CPF</label>
            <input type="text" name="cpf" class="form-control" id="#" placeholder="000.000.000-00">
            </div>

            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">E-mail</label>
                <input type="email" class="form-control" id="inputEmail4" placeholder="Digite o seu E-mail">
            </div>
            <div class="col-md-6">
                <label for="inputPassword4" class="form-label">Senha</label>
                <input type="password" class="form-control" id="inputPassword4" placeholder="Digite a sua Senha">
            </div>
            <div class="col-12">
                <label for="inputAddress" class="form-label">Bairro</label>
                <input type="text" class="form-control" id="inputAddress" placeholder="Digite o seu Bairro">
            </div>

            <div class="col-md-6">
            <label for="inputRua" class="form-label">Rua</label>
            <input type="text" class="form-control" id="#" placeholder="Digite a sua Rua">
            </div>

            <div class="col-md-6">
                <label for="inputCity" class="form-label">Cidade</label>
                <input type="text" class="form-control" id="inputCity">
            </div>
            <div class="col-md-4">
                <label for="inputState" class="form-label">UF</label>
                <select id="inputState" class="form-select">
                    <option selected>SP</option>
                </select>
            </div>

            <div class="col-md-3">
            <label for="inputrg" class="form-label">RG</label>
            <input type="text" name="rg" class="form-control" id="#" placeholder="00.000.000-0">
            </div>

            <div class="col-md-4">
            <label for="inputcep" class="form-label">CEP</label>
            <input type="text" name="cep" class="form-control" id="#" placeholder="00000-000">
            </div>

            <div class="col-md-2">
            <label for="inputCasa" class="form-label">Número</label>
            <input type="text" name="casa" class="form-control" id="#" placeholder="Ex: 565">
            </div>

            <div class="col-12">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck">
                    <label class="form-check-label" for="gridCheck">
                        Lembre-me
                    </label>
                </div>
            </div>
            <div class="col-12">
                <button type="submit" class="btn" style="background-color: #836FFF;">Prosseguir</button>
            </div>
        </form>



    </main>







        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</body>

</html>
