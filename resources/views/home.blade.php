<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prego Digital</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="resources/css/cadastro.css">
</head>

<body>
    <header class="container fixed-top">

        <nav class="navbar navbar-expand-lg" style="background-color: #6959CD;">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php">
                    <img src="assets/img/logoPD.png" width="45" height="45" class="d-inline-block align-top" alt="">
                    <strong class="text-brand">Prego Digital</strong>
                </a>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarText">

                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="">Sobre Nós</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="">Contate-Nos</a>
                        </li>

                    </ul>

                    <span class="sessao-usuario">
                        <a class="btn" style="background-color: #836FFF;" href="cadastro.php">Crie uma conta</a>
                        <a class="btn" style="background-color: #836FFF;" href="login.php">Entrar</a>
                    </span>


                </div>
            </div>
        </nav>

        <br>
    </header>


        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</body>

</html>
